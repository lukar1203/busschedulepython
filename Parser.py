#!/usr/bin/python
# -*- coding: utf-8 -*-
import csv

import re
import logging
import xlrd
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from create_db_schema import Base, BusStop, Departure, Bus, Route, Key

engine = create_engine('sqlite:///bus_schedule.db')
Base.metadata.bind = engine

DBSession = sessionmaker(bind=engine)
session = DBSession()


class Parser:
    def __init__(self, file_name):
        self.book = xlrd.open_workbook(file_name)

    @staticmethod
    def cell_value_to_string(j, i, sheet):
        return str(int(sheet.cell_value(j, i))) if isinstance(sheet.cell_value(j, i),
                                                              float) else sheet.cell_value(j, i)

    @staticmethod
    def contains_digits(string):
        return any(char.isdigit() for char in string)

    def parse_regular_bus_stop(self, sheet, sheet_row_values):
        sheet_row_values = [str(int(elem)) if isinstance(elem, float) else elem for elem in sheet_row_values]
        sheet_row_values = [sheet.row_values(2)[0]] + sheet_row_values if not self.contains_digits(
            sheet_row_values[0]) else sheet_row_values
        sheet_row_values = filter(lambda x: 'dnia' not in x, sheet_row_values)
        number = sheet_row_values[0]
        number = (re.findall('\d+', number) if not 'M' in number else [number])
        return number, sheet_row_values

    @staticmethod
    def parse_double_bus_stops(sheet, sheet_row_values):
        sheet_row_values = [str(int(elem)) if isinstance(elem, float) else elem for elem in sheet_row_values]
        sheet_row_values = sheet_row_values + [sheet_row_values[1]] if len(sheet_row_values) < 4 else sheet_row_values
        sheet_row_values[1] = sheet_row_values[1] + ' ( ' + sheet.row_values(2)[0].replace('\n', '') + ' )'
        logging.info(sheet_row_values[0] + ' ' + sheet_row_values[1])
        new_bus_stop = BusStop(number=sheet_row_values[0], name=sheet_row_values[1])
        session.add(new_bus_stop)

        second_bus_stop_index = sheet.row_values(1).index(sheet_row_values[2])
        sheet_row_values[3] = sheet_row_values[3] + ' ( ' + sheet.row_values(2)[second_bus_stop_index].replace(
            '\n', '') + ' )'
        logging.info(sheet_row_values[2] + ' ' + sheet_row_values[3])
        new_bus_stop = BusStop(number=sheet_row_values[2], name=sheet_row_values[3])
        session.add(new_bus_stop)

    def read_bus_stops(self):
        session.query(BusStop).filter().delete()
        values = []
        for j in range(3, self.book.nsheets):
            sheet = self.book.sheet_by_index(j)
            if 'O' not in sheet.name and 'o' not in sheet.name and sheet.name != '11' \
                    and ('(' not in sheet.name or 'RETRO' in sheet.name):
                sheet_row_values = sheet.row_values(1)
                sheet_row_values = filter(None, sheet_row_values)

                if ' ' not in sheet.name and ',' not in sheet.name and 'ZDZ' not in sheet.name:
                    number, sheet_row_values = self.parse_regular_bus_stop(sheet, sheet_row_values)
                    logging.info(number[0] + ' ' + sheet_row_values[1])
                    new_bus_stop = BusStop(number=number[0], name=sheet_row_values[1])
                    session.add(new_bus_stop)
                elif 'ZDZ' in sheet.name:
                    logging.info(sheet_row_values[0])
                    new_bus_stop = BusStop(number='ZDZ',
                                           name=sheet_row_values[0].encode('ascii', 'ignore').decode('ascii'))
                    session.add(new_bus_stop)
                else:
                    self.parse_double_bus_stops(sheet, sheet_row_values)

                values.append(sheet_row_values)

        session.commit()
        return values

    def read_departure_times(self):
        session.query(Departure).filter().delete()
        session.query(Bus).filter().delete()
        values = []
        for j in range(3, self.book.nsheets - 1):
            sheet = self.book.sheet_by_index(j)

            if not 'O' in sheet.name and not 'o' in sheet.name and not '(' in sheet.name and not 'ZDZ' in sheet.name:
                indices = [i for i, x in enumerate(sheet.col_values(0)) if x == 'GODZ.']
                for i in range(len(indices)):
                    self.parse_day_tables(indices[i], sheet, i)
            elif 'ZDZ' in sheet.name:
                self.parse_ZDZ(sheet)

        return values

    def parse_day_tables(self, n, sheet, day_type):
        occurrences = [i for i, x in enumerate(sheet.row_values(n)) if x == 'GODZ.']
        if not ' ' in sheet.name and not ',' in sheet.name:
            logging.info(sheet.name)
            number = str(int(sheet.name)) if isinstance(sheet.name, float) else sheet.name
            for occurrence in occurrences:
                self.parse_table(occurrence, n + 1, sheet, number, day_type)
        else:
            number = sheet.name.replace(',', ' ').split()
            logging.info(number)
            self.parse_table(occurrences[0], n + 1, sheet, number[0], day_type)
            self.parse_table(occurrences[1], n + 1, sheet, number[1], day_type)

    def parse_table(self, x, y, sheet, number, day_type):
        i = x + 1
        bus_number = self.cell_value_to_string(y - 1, i, sheet)
        bus_number = bus_number.replace(' ', '')

        stop = session.query(BusStop).filter(BusStop.number == number).first()
        y = y - 1 if not sheet.cell_value(y, x) == "" else y

        while bus_number.isalnum():
            new_bus = Bus(bus_number=bus_number, bus_stop_id=stop.id)
            session.add(new_bus)
            j = y + 1
            hour = self.cell_value_to_string(j, x, sheet)

            while hour.isdigit():
                minutes = self.cell_value_to_string(j, i, sheet)
                new_departure = Departure(hour=hour, minutes=minutes, day_type=day_type,
                                          bus_stop_id=stop.id, bus_number=bus_number)
                session.add(new_departure)
                j += 1

                if j < len(sheet.col_values(x)):
                    hour = self.cell_value_to_string(j, x, sheet)
                else:
                    hour = ''

            i += 1

            if i < len(sheet.row_values(y - 1)):
                bus_number = self.cell_value_to_string(y if not sheet.cell_value(y, x) == "" else y - 1, i, sheet)
                bus_number = bus_number.replace(' ', '')
            else:
                bus_number = ''

        session.commit()

    def parse_ZDZ(self, sheet):
        x = 0
        y = sheet.col_values(0).index('GODZ.') + 1
        cols_to_read = [1, 2, 4]
        for i in cols_to_read:
            bus_number = u'2' if i == 2 else self.cell_value_to_string(y - 1, i, sheet)
            stop = session.query(BusStop).filter(BusStop.number == 'ZDZ').first()

            new_bus = Bus(bus_number=bus_number, bus_stop_id=stop.id)
            session.add(new_bus)
            j = y + 1
            hour = self.cell_value_to_string(j, x, sheet)

            while hour.isdigit():
                if i != 2:
                    minutes = self.cell_value_to_string(j, i, sheet)
                    new_departure = Departure(hour=hour, minutes=minutes, day_type=0,
                                              bus_stop_id=stop.id, bus_number=bus_number)
                    session.add(new_departure)
                else:
                    if hour == '13':
                        minutes = self.cell_value_to_string(j, i, sheet) + u' ( k. Pozna\xc5ska )'
                        new_departure = Departure(hour=hour, minutes=minutes, day_type=0,
                                                  bus_stop_id=stop.id, bus_number=bus_number)
                        session.add(new_departure)
                    if hour == "14":
                        minutes = self.cell_value_to_string(j, i + 1, sheet) + u' ( k. W. Polskiego )'
                        new_departure = Departure(hour=hour, minutes=minutes, day_type=0,
                                                  bus_stop_id=stop.id, bus_number=bus_number)
                        session.add(new_departure)
                j += 1
                if j < len(sheet.col_values(x)):
                    hour = self.cell_value_to_string(j, x, sheet)
                else:
                    hour = ''


def parse_key(file_name):
    rows = csv.reader(open(file_name, 'rb'))
    session.query(Key).filter().delete()

    for row in rows:
        row = row[0].split(';')
        symbol = row[0].decode('cp1250')
        definition = row[1].decode('cp1250')
        definition = definition[0:1].capitalize() + definition[1:]
        new_key = Key(symbol=symbol, definition=definition)
        session.add(new_key)

    session.commit()


def parse_routes(file_name):
    rows = csv.reader(open(file_name, 'rb'))
    session.query(Route).filter().delete()

    for row in rows:
        row = row[0].split(';')
        number = row[0].decode('utf-8')
        description = row[1].decode('utf-8')
        new_route = Route(symbol=number, definition=description)
        session.add(new_route)

    session.commit
