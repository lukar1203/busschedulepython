#!/usr/bin/python
# -*- coding: utf-8 -*-

from flask import Flask, jsonify, after_this_request, request, g
from time import time
from werkzeug.contrib.cache import SimpleCache
from Parser import Parser, parse_key, parse_routes
import Reader
import pickle
import os
import logging
from cStringIO import StringIO as IO
import gzip
import functools

FILE_NAME = 'db_version.p'

app = Flask(__name__)
cache = SimpleCache()

if os.environ['SERVER'] == 'debug':
    path = ''
    logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG)
else:
    path = '/home/lukar1203/bus_schedule/'
    logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.WARNING)


def gzipped(f):
    @functools.wraps(f)
    def view_func(*args, **kwargs):
        @after_this_request
        def zipper(response):
            accept_encoding = request.headers.get('Accept-Encoding', '')

            if 'gzip' not in accept_encoding.lower():
                return response

            response.direct_passthrough = False

            if (response.status_code < 200 or
                        response.status_code >= 300 or
                        'Content-Encoding' in response.headers):
                return response
            gzip_buffer = IO()
            gzip_file = gzip.GzipFile(mode='wb',
                                      fileobj=gzip_buffer)
            gzip_file.write(response.data)
            gzip_file.close()

            response.data = gzip_buffer.getvalue()
            response.headers['Content-Encoding'] = 'gzip'
            response.headers['Vary'] = 'Accept-Encoding'
            response.headers['Content-Length'] = len(response.data)

            return response

        return f(*args, **kwargs)

    return view_func


@app.route('/update')
def update():
    parse_key(path + 'key.csv')
    parse_routes(path + 'routes.csv')
    parser = Parser(path + 'MIEJSKIE-05.08.2017.xlsx')
    parser.read_bus_stops()
    parser.read_departure_times()
    time_stamp = time()
    pickle.dump(time_stamp, open(FILE_NAME, 'wb'))
    cache_response()
    return '<h1>Update successful</h1>'


@app.route('/bus-stops')
def retrieve_bus_stop_table():
    return jsonify(list=Reader.read_bus_stops())


@app.route('/departures')
def retrieve_departure_table():
    return jsonify(list=Reader.read_departures())


@app.route('/buses')
def retrieve_bus_table():
    return jsonify(list=Reader.read_buses())


@app.route('/keys')
def retrieve_key_table():
    return jsonify(list=Reader.read_keys())


@app.route('/routes')
def retrieve_route_table():
    return jsonify(list=Reader.read_routes())


def cache_response():
    response = jsonify(routes=Reader.read_routes(),
                       keys=Reader.read_keys(),
                       bus_stops=Reader.read_bus_stops(),
                       departures=Reader.read_departures(),
                       buses=Reader.read_buses())
    cache.set('response', response)
    return response


@app.route('/')
@gzipped
def retrieve_all():
    response = cache.get('response')
    if response is None:
        response = cache_response()

    return response


@app.route('/check-updates')
def check_updates():
    return jsonify(time_stamp=pickle.load(open(FILE_NAME, 'rb')))


@app.before_request
def before_request():
    g.start = time()


@app.teardown_request
def teardown_request(exception=None):
    diff = time() - g.start
    print(diff)


if __name__ == '__main__':
    app.run()
