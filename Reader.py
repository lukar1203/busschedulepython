#!/usr/bin/python
# -*- coding: utf-8 -*-
import pickle
import cPickle

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from create_db_schema import Base, BusStop, Departure, Bus, Key, Route


engine = create_engine('sqlite:///bus_schedule.db')
Base.metadata.bind = engine

DBSession = sessionmaker(bind=engine)
session = DBSession()


def read_bus_stops():
    cols = ['id', 'number', 'name']
    data = session.query(BusStop).all()
    result = [{col: getattr(d, col) for col in cols} for d in data]
    return result


def read_buses():
    cols = ['id', 'bus_number', 'bus_stop_id']
    data = session.query(Bus).all()
    result = [{col: getattr(d, col) for col in cols} for d in data]
    return result


def read_departures():
    cols = ['id', 'hour', 'minutes', 'day_type', 'bus_number', 'bus_stop_id']
    data = session.query(Departure).all()
    result = [{col: getattr(d, col) for col in cols} for d in data]
    return result


def read_keys():
    cols = ['id', 'symbol', 'definition']
    data = session.query(Key).all()
    result = [{col: getattr(d, col) for col in cols} for d in data]
    return result


def read_routes():
    cols = ['id', 'symbol', 'definition']
    data = session.query(Route).all()
    result = [{col: getattr(d, col) for col in cols} for d in data]
    return result
