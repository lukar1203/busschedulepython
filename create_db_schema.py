from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import create_engine

Base = declarative_base()


class BusStop(Base):
    __tablename__ = 'busstop'
    id = Column(Integer, primary_key=True)
    number = Column(String(10))
    name = Column(String(100))


class Bus(Base):
    __tablename__ = 'bus'
    id = Column(Integer, primary_key=True)
    bus_number = Column(String(20))
    bus_stop_id = Column(Integer, ForeignKey('busstop.id'))
    busstop = relationship(BusStop)


class Departure(Base):
    __tablename__ = 'departure'
    id = Column(Integer, primary_key=True)
    hour = Column(Integer)
    minutes = Column(Integer)
    day_type = Column(Integer)
    bus_stop_id = Column(Integer, ForeignKey('busstop.id'))
    busstop = relationship(BusStop)
    bus_number = Column(Integer)


class Key(Base):
    __tablename__ = 'key'
    id = Column(Integer, primary_key=True)
    symbol = Column(String(5))
    definition = Column(String(50))


class Route(Base):
    __tablename__ = 'route'
    id = Column(Integer, primary_key=True)
    symbol = Column(String(10))
    definition = Column(String(1000))


engine = create_engine('sqlite:///bus_schedule.db', convert_unicode=True)
Base.metadata.create_all(engine)